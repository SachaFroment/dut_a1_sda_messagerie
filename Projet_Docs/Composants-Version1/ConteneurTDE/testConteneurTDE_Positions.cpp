/**
 * @file testConteneurTDE_Positions.cpp
 * Projet sem04-tp-Cpp2
 * @author l'�quipe p�dagogique 
 * @version 2 - 26/11/11
 * @brief Test d'un conteneur de positions 
 * de type tableau � capacit� extensible suivant un pas d'extension
 * Structures de donn�es et algorithmes - DUT1 Paris 5
 */

#include <iostream>
using namespace std;

#include "ConteneurTDE.h"

/* Test d'un conteneur (de type ConteneurTDE) de positions */ 
int main() {
	ConteneurTDE cPositions; // D�claration du conteneur de positions test�
	unsigned int nbPositions=0;// Nb de positions enregistr�es dans le conteneur
	Position p;
	bool estOrigine;
	
	initialiser(cPositions, 1, 2); 
	
	/* Remplir le conteneur de positions 
	 * jusqu'� la saisie de la position origine (non enregistr�e) */	
	unsigned int i = 0;
	cout << "Saisir des positions jusqu'� la saisie de l'origine (0,0)\n";
	cout << "Les positions (� l'exception de l'origine)\n";
	cout << "seront enregistr�es dans le conteneur dynamique" << endl;
	do {
		p = saisir();
		estOrigine = (p.abscisse == 0) && (p.ordonnee == 0);
		if (!estOrigine) {
			ecrire(cPositions, i++, p); 
			nbPositions++;
		}
	} while (!estOrigine);
	
	/* Afficher la capacit� du conteneur de positions */	
	cout << "Capacit� du conteneur dynamique : " << cPositions.capacite << endl;
	
	/* Afficher le conteneur de positions */
		cout << "Conteneur allou� en m�moire dynamique de : " << nbPositions 
			 << " position(s)" << endl; 
		for (unsigned int i = 0; i < nbPositions; ++i) {
			p = lire(cPositions, i);
			afficher(p);
		}
	
	// D�sallocation du conteneur
	detruire(cPositions);
	
	return 0;
}
