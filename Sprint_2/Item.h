#ifndef _ITEM_
#define _ITEM_

/**
* @file Item.h
* Projet Sprint 2
* @author Abelhaj Youssef et Froment Sacha G106
* @version 1 - 14/12/2017
* @brief Spécialisation du type Item
* Structures de données et algorithmes - DUT1 Paris 5
*/

#include "IdentificateurMessage.h"

typedef IdMessage Item;

#endif
