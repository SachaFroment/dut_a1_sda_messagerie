#ifndef _MESSAGERIE_
#define _MESSAGERIE_

/**
* @file Messagerie.h
* Projet Sprint 5
* @author Abelhaj Youssef et Froment Sacha G106
* @version 1 - 14/12/2017
* @brief Composant de la messagerie
* Structures de donn�es et algorithmes - DUT1 Paris Descartes
*/

#include "PaquetReseau.h"
#include "Liste.h"

/**
* @brief Structure de donn�es de type Messagerie
*/

struct  Messagerie{
	enum { MAX = 21 };
	Liste listeM;
};

/** 
* @brief allouer et d�sallouer la variable m de type Messagerie 
* @param[in] m : la messagerie � initialiser
*/
void initialiser(Messagerie& m);
void detruire(Messagerie& m);
/**
* @brief Reception d'un paquet-r�seau
* @param[in-out] is : le flot d'entr�e
* @param[in] pr : le Paquet r�seau saisit
*/
void recevoirPaquetReseau(std::istream& is, PaquetReseau& pr);

/**
* @brief Organiser un paquet-r�seau dans la messagerie 
* @param[in-out] is : le flot d'entr�e
* @param[in] m : la messagerie � organiser
* @param[in] mc : le message en cours de reception
* @param[in] pr : le Paquet r�seau � traiter
*/
void traiterPaquetReseau(Messagerie& m, MessageEnCours& mc, PaquetReseau& pr);

/**
* @brief Affichage d'un paquet-r�seau
* @param[in-out] os : le flot de sortie
* @param[in] m : la messagerie � afficher
*/
void afficher_mess(std::ostream& os, Messagerie& m);

#endif