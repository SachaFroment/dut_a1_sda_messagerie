#ifndef _ITEM_2_
#define _ITEM_2_

/**
* @file Item.h
* Projet sem04-tp-Cpp3
* @author l'�quipe p�dagogique
* @version 1 23/12/05
* @brief Sp�cialisation du type Item
* Structures de donn�es et algorithmes - DUT1 Paris 5
*/

#include "BlocData.h"
typedef BlocData Item2;

#endif