#ifndef _ITEM_F_
#define _ITEM_F_

/**
* @file ItemF.h
* Projet Sprint 6
* @author Abelhaj Youssef et Froment Sacha G106
* @version 1 - 14/12/2017
* @brief Spécialisation du type Item
* Structures de données et algorithmes - DUT1 Paris 5
*/

#include "BlocData.h"

typedef BlocData ItemF;

#endif
